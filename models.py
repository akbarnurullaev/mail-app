from fastapi_utils.guid_type import GUID_DEFAULT_SQLITE, GUID
from sqlalchemy import Column, Text

from database import Base


class Prompt(Base):
    __tablename__ = 'prompts'
    id = Column(GUID, primary_key=True, default=GUID_DEFAULT_SQLITE)
    text = Column(Text, nullable=False)
