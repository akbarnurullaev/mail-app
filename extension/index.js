const xpaths = {
    actionBar: 'td.gU.Up',
    replyInput: '//*[@id=":8g"]',
    history: '/html/body/div[7]/div[3]/div/div[2]/div[2]/div/div/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div[2]/div/div[3]/div[5]/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div',
}
const buttonText = 'AI Reply'
const gptButtonId = 'GPT_GENERATION'
const url = 'https://aireply.pro'
// const url = 'http://127.0.0.1:8000'
const styles = `
    <style>
     .custom-extension-button-by-akbar {
         z-index: 9999;
         position: relative;
         background-color: #0b57d0;
         border: none;
         border-radius: 18px;
         padding: 8px 16px;
         margin-left: 8px;
         color: white;
         font-family: "Google Sans",Roboto,RobotoDraft,Helvetica,Arial,sans-serif;
         cursor: pointer;
         transition-duration: 0.2s;
     }
     .custom-extension-button-by-akbar:hover {
         box-shadow: 0 1px 2px 0 rgba(26,115,232,0.451), 0 1px 3px 1px rgba(26,115,232,0.302);
         background-color: #297be6;
         outline: 1px solid transparent;
         transition-duration: 0.2s;
     }
     .custom-extension-button-by-akbar:active {
         box-shadow: none;
         background-color: #0b57d0;
         transition-duration: 0.2s;
     }
     
     .dot-flashing {
      position: relative;
      width: 4px;
      height: 4px;
      border-radius: 5px;
      background-color: white;
      color: white;
      animation: dot-flashing 1s infinite linear alternate;
      animation-delay: 0.5s;
    }
    .dot-flashing::before, .dot-flashing::after {
      content: "";
      display: inline-block;
      position: absolute;
      top: 0;
    }
    .dot-flashing::before {
      left: -8px;
      width: 4px;
      height: 4px;
      border-radius: 5px;
      background-color: white;
      color: white;
      animation: dot-flashing 1s infinite alternate;
      animation-delay: 0s;
    }
    .dot-flashing::after {
      left: 8px;
      width: 4px;
      height: 4px;
      border-radius: 5px;
      background-color: white;
      color: white;
      animation: dot-flashing 1s infinite alternate;
      animation-delay: 1s;
    }
    
    @keyframes dot-flashing {
      0% {
        background-color: white;
      }
      50%, 100% {
        background-color: rgba(0,0,0,0.13);
      }
    }
  </style>
`


document.head.insertAdjacentHTML("beforeend", styles)
document.addEventListener('DOMNodeInserted', () => {
    const actionBar = document.querySelector(xpaths.actionBar)
    const gptButton = document.querySelector(`#${gptButtonId}`)

    if (actionBar && !gptButton) {
        const button = new Button()

        actionBar.appendChild(button)
        button.parentElement.style.display = 'flex'
    }
});

class Button {
    constructor() {
        if (Button._instance) {
            return Button._instance
        }
        Button._instance = this;

        const button = document.createElement('button')

        button.className = 'custom-extension-button-by-akbar'
        button.style.zIndex = '9999'
        button.style.position = 'relative'
        button.innerText = buttonText
        button.id = gptButtonId
        button.onclick = this.generateResponse

        return button
    }

    async generateResponse() {
        const subject = document.querySelector('.hP').textContent || ""
        const receiver = document.querySelector(".gD").textContent || "the people"
        const me =
            document
                ?.querySelector('.gb_d.gb_Fa.gb_J')
                ?.getAttribute("aria-label")
                ?.split(':')?.[1] || ""
        const input = document.querySelector('.editable')
        let history = document.querySelectorAll('.gs')
        history = history[history.length - 1]

        await toggleLoading(async () => {
            const response = await fetch(`${url}/set_mail`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": url,
                },
                body: JSON.stringify({history: history?.textContent, receivers: [receiver], me, subject}),
            })
            const mailId = await response.json()
            const data = new EventSource(`${url}/streaming_gpt?id_=${mailId}`)

            data.onopen = () => {
                console.log('OPEN')
                input.innerText = ""
            }
            data.onmessage = (ev) => {
                if (ev.data.includes('$ended$')) {
                    data.close()
                    if (input.innerText.slice(-1) !== '.') {
                        input.innerText += '.'
                    }
                } else {
                    if (/\s/.test(input.innerText.charAt(0))) {
                        input.innerText = input.innerText.substring(1)
                    }
                    input.innerText += Array.from(ev.data).every((char) => char === ' ') ? ev.data.replace(' ', '\u00A0') : ev.data.replaceAll('^', '\n')
                }

                const scroll = document.querySelector(".qz")
                scroll.scrollTop = scroll.scrollHeight
            };
        })
    }
}

async function toggleLoading(func) {
    const button = document.querySelector(`#${gptButtonId}`)

    button.disabled = true
    button.innerHTML = `
            <div class="snippet" data-title="dot-flashing">
                <div class="stage">
                    <div class="dot-flashing"></div>
                </div>
            </div>
        `
    await func()
    button.disabled = false
    button.innerText = buttonText
}