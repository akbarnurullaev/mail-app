import {Button, FormControl, FormLabel} from "@mui/joy";
import Input from "@mui/joy/Input";
import Typography from "@mui/joy/Typography";
import Sheet from "@mui/joy/Sheet";
import {useForm} from "react-hook-form";
import {useAuthentication} from "../api/hooks/useAuthentication.ts";

export const Login = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: {isValid}
  } = useForm<{ username: string, password: string }>();
  const {mutate, isLoading} = useAuthentication();

  const onSubmit = handleSubmit( async (formData) => {
    try {
      await mutate(formData);
    } catch (e) {
      console.log(e);
    }
  });

  return (
    <div>
      <Sheet
        sx={{
          width: 300,
          mx: "auto",
          my: 4,
          py: 3,
          px: 2,
          display: "flex",
          flexDirection: "column",
          gap: 2,
          borderRadius: "sm",
          boxShadow: "md",
        }}
      >
        <Typography level="h4">
            Welcome!
        </Typography>
        <Typography level="body2">Sign in to continue.</Typography>
        <FormControl>
          <FormLabel>Email</FormLabel>
          <Input
            placeholder="username"
            {...register("username", {required: true})}
          />
        </FormControl>
        <FormControl>
          <FormLabel>Password</FormLabel>
          <Input
            type="password"
            placeholder="password"
            {...register("password", {required: true})}
          />
        </FormControl>
        <Button
          onClick={onSubmit}
          disabled={!isValid}
          loading={isLoading}
          sx={{ mt: 1 /* margin top */ }}
        >
          Log in
        </Button>
      </Sheet>
    </div>
  );
};
