import {api} from "../config.ts";
import {useInfiniteQuery} from "@tanstack/react-query";

export type Mail = {
    "id": string,
    "subject": string,
    "body": string,
    "date": string,
    "html": string,
    "from": string,
    "to": string[],
    "cc": string[],
}

type MailsResponse = {
    "data": Mail[],
    "total": number,
    "pages_count": number
    "page": number
}

export const QUERY_KEY = ["Mails"];

const fetchMails = async ({pageParam = 0}): Promise<MailsResponse> => {
  const {data} = await api.get(`/mails?per_page=${10}&page=${pageParam}`);
  return data;
};

export const useMailsQuery = () => {
  return useInfiniteQuery<MailsResponse, Error>({
    queryKey: QUERY_KEY,
    queryFn: fetchMails,
    getNextPageParam: (lastPage) => lastPage.page + 1,
  });
};
