import {useMutation} from "@tanstack/react-query";
import {MailInput} from "../../components/WriteEmail.tsx";
import {Mail} from "./useMails.ts";
import {api} from "../config.ts";


const parseReplyingMaiBody = (html: string) => {
  const htmlParser = new DOMParser();
  const parsedMailHtml = htmlParser.parseFromString(html, "text/html");

  const history = parsedMailHtml.querySelector(".gmail_quote")!.outerHTML;
  const text = parsedMailHtml.querySelector("div")!.innerText;
  return {
    text,
    history
  };
};

export const useComposeMail = (onSuccess?: () => void) => {
  return useMutation({
    onSuccess,
    mutationFn: (mail: MailInput & { parentMail?: Mail }) => {
      console.log(mail.parentMail);
      if (mail.parentMail) {
        const {text, history} = parseReplyingMaiBody(mail.parentMail.html);

        mail.subject = mail.subject?.startsWith("Re") ? mail.subject : `Re: ${mail.subject}`;
        mail.body = `
        <html>
          <head></head>
          <body>
            <div dir="ltr">${mail.body}</div>
            <br>
            <div class="gmail_quote">
              <div dir="ltr" class="gmail_attr">${new Date(mail.parentMail.date).toDateString()} &lt;<a href="mailto:akbarnurullayev24@gmail.com">${mail.parentMail.from}</a>&gt; wrote:<br></div>
              <blockquote class="gmail_quote" style="margin:0 0 0 0.8ex;border-left:1px solid rgb(204,204,204);padding-left:1ex">
                <div dir="ltr">${text}</div>
                <br>
                ${history}
              </blockquote>
            </div>
          </body>
        </html>
      `;

        return api.post(`/reply?mail_id=${mail.parentMail.id}`, mail);
      }

      return api.post("/compose", mail);
    },
  });
};