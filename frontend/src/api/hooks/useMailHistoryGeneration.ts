import {useMutation} from "@tanstack/react-query";
import {api} from "../config.ts";


export const useMailHistoryGeneration = () => {
  return useMutation({
    mutationFn: (mailHistory: string) => {
      return api.post("/reply_gpt", {history: mailHistory});
    },
  });
};