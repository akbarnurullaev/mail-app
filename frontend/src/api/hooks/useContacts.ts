import {api} from "../config.ts";
import {useQuery} from "@tanstack/react-query";

type ContactsResponse = string[]

export const QUERY_KEY = ["Contacts"];

const fetchContacts = async (): Promise<ContactsResponse> => {
  const {data} = await api.get("/contacts");
  return data;
};

export const useContactsQuery = () => {
  return useQuery<ContactsResponse, Error>({
    queryKey: QUERY_KEY,
    queryFn: fetchContacts,
  });
};
