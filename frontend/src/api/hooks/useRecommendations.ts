import {api} from "../config.ts";
import {useQuery} from "@tanstack/react-query";
import {Mail} from "./useMails.ts";

export type Recommendation = {
    "gpt"?: string,
} & Mail

export type GptResponse = {
    actionItems: string[]
    overview: string
    urgent: boolean
    important: boolean
    response: string
}

type RecommendationsResponse = Recommendation[]

export const QUERY_KEY = ["Recommendations"];

const fetchRecommendations = async (): Promise<RecommendationsResponse> => {
  const {data} = await api.get("/recommendations");
  return data;
};

export const useRecommendationsQuery = () => {
  return useQuery<RecommendationsResponse, Error>({
    queryKey: QUERY_KEY,
    queryFn: fetchRecommendations,
    refetchInterval: 5 * 60 * 1000,
  });
};
