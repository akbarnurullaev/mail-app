import {createContext, useContext, useState} from "react";
import {Children} from "../types.ts";
import {Mail} from "../api/hooks/useMails.ts";
import WriteEmail, {MailInput} from "../components/WriteEmail.tsx";
import {FocusTrap} from "@mui/base";

export type ComposeContextContextValue = {
    composeMail: (mail: MailInput) => void
    close: () => void
    open: () => void
    setSelectedMail: (mail: Mail) => void
    selectedMail?: Mail
}

const ComposeContext = createContext<ComposeContextContextValue | null>(null);

export const ComposeProvider = ({children}: Children) => {
  const [isOpened, setIsOpened] = useState(false);
  const [selectedMail, setSelectedMail] = useState<undefined | Mail>();
  const [mail, setMail] = useState<MailInput>();

  const composeMail: ComposeContextContextValue["composeMail"] = ({subject, body, to, cc, reply}) => {
    setIsOpened(true);
    setMail({subject, body, cc, to, reply});
  };

  const close = () => {
    setIsOpened(false);
  };

  const open = () => {
    setIsOpened(true);
  };

  const value: ComposeContextContextValue = {
    composeMail,
    setSelectedMail,
    selectedMail,
    close,
    open,
  };

  return (
    <ComposeContext.Provider value={value}>
      {children}
      <FocusTrap open={isOpened} disableAutoFocus disableEnforceFocus>
        <WriteEmail open={isOpened} onClose={() => setIsOpened(false)} mail={mail}
          parentMail={selectedMail}/>
      </FocusTrap>
    </ComposeContext.Provider>
  );
};

export const useCompose = () => {
  return useContext(ComposeContext);
};
