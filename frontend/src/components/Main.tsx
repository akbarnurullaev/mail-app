import {Button} from "@mui/joy";
import Layout from "./Layout";
import Navigation from "./Navigation.tsx";
import Stack from "@mui/joy/Stack";
import Header from "./Header.tsx";
import EmailRoundedIcon from "@mui/icons-material/EmailRounded";
import PeopleAltRoundedIcon from "@mui/icons-material/PeopleAltRounded";
import FolderRoundedIcon from "@mui/icons-material/FolderRounded";
import {Route, Routes} from "react-router-dom";
import {EmailsBlock} from "./EmailsBlock.tsx";
import {RecommendationsBlock} from "./RecommendationsBlock.tsx";
import {Analytics} from "./Analytics.tsx";


export function Main() {
  return (
    <>
      <Stack
        id="tab-bar"
        direction="row"
        justifyContent="space-around"
        spacing={1}
        sx={{
          display: {xs: "flex", sm: "none"},
          zIndex: "999",
          bottom: 0,
          position: "fixed",
          width: "100dvw",
          py: 2,
          backgroundColor: "background.body",
          borderTop: "1px solid",
          borderColor: "divider",
        }}
      >
        <Button
          variant="plain"
          color="neutral"
          aria-pressed="true"
          component="a"
          href="/joy-ui/getting-started/templates/email/"
          size="sm"
          startDecorator={<EmailRoundedIcon/>}
          sx={{flexDirection: "column", "--Button-gap": 0}}
        >
                    Email
        </Button>
        <Button
          variant="plain"
          color="neutral"
          component="a"
          href="/joy-ui/getting-started/templates/team/"
          size="sm"
          startDecorator={<PeopleAltRoundedIcon/>}
          sx={{flexDirection: "column", "--Button-gap": 0}}
        >
                    Team
        </Button>
        <Button
          variant="plain"
          color="neutral"
          component="a"
          href="/joy-ui/getting-started/templates/files/"
          size="sm"
          startDecorator={<FolderRoundedIcon/>}
          sx={{flexDirection: "column", "--Button-gap": 0}}
        >
                    Files
        </Button>
      </Stack>
      <Layout.Root>
        <Layout.Header>
          <Header/>
        </Layout.Header>
        <Layout.SideNav>
          <Navigation/>
        </Layout.SideNav>
        <Routes>
          <Route path="/" element={<EmailsBlock/>}/>
          <Route path="/recommendations" element={<RecommendationsBlock/>}/>
          <Route path="/analytics" element={<Analytics/>}/>
        </Routes>
      </Layout.Root></>
  );
}
