import * as React from "react";
import Box from "@mui/joy/Box";
import Typography from "@mui/joy/Typography";
import List from "@mui/joy/List";
import ListDivider from "@mui/joy/ListDivider";
import ListItem from "@mui/joy/ListItem";
import ListItemButton, {listItemButtonClasses} from "@mui/joy/ListItemButton";
import {Mail, useMailsQuery} from "../api/hooks/useMails.ts";
import {truncate} from "../helpers/truncate.ts";
import {Button, CircularProgress, LinearProgress} from "@mui/joy";
import {Refresh} from "@mui/icons-material";
import CreateRoundedIcon from "@mui/icons-material/CreateRounded";
import {useCompose} from "../providers/ComposeContext.tsx";

// const data = [
//   {
//     name: 'Alex Jonnold',
//     avatar: 'https://i.pravatar.cc/40?img=3',
//     avatar2x: 'https://i.pravatar.cc/80?img=3',
//     date: '21 Oct 2022',
//     title: 'Details for our Yosemite Park hike',
//     body: 'Hello, my friend! So, it seems that we are getting there…',
//     color: 'warning.400',
//   },
//   {
//     name: 'Pete Sand',
//     avatar: 'https://i.pravatar.cc/40?img=4',
//     avatar2x: 'https://i.pravatar.cc/80?img=4',
//     date: '06 Jul 2022',
//     title: 'Tickets for our upcoming trip',
//     body: 'Good day, mate! It seems that our tickets just arrived…',
//     color: 'success.400',
//   },
//   {
//     name: 'Kate Gates',
//     avatar: 'https://i.pravatar.cc/40?img=5',
//     avatar2x: 'https://i.pravatar.cc/80?img=5',
//     date: '16 May 2022',
//     title: 'Brunch this Saturday?',
//     body: "Hey! I'll be around the city this weekend, how about a…",
//     color: 'primary.500',
//   },
//   {
//     name: 'John Snow',
//     avatar: 'https://i.pravatar.cc/40?img=7',
//     avatar2x: 'https://i.pravatar.cc/80?img=7',
//     date: '10 May 2022',
//     title: 'Exciting News!',
//     body: 'Hello there! I have some exciting news to share with you...',
//     color: 'danger.500',
//   },
//   {
//     name: 'Michael Scott',
//     avatar: 'https://i.pravatar.cc/40?img=8',
//     avatar2x: 'https://i.pravatar.cc/80?img=8',
//     date: '13 Apr 2022',
//     title: 'Upcoming Product Launch',
//     body: 'Dear customers and supporters, I am thrilled to announc...',
//     color: 'danger.500',
//   },
// ];

export default function EmailList({chooseEmail, selectedMail}: {
    chooseEmail: (mail: Mail) => () => void,
    selectedMail?: Mail,
}) {
  const mailManager = useCompose();
  const {data, isLoading, isRefetching, isFetchingNextPage, hasNextPage, fetchNextPage, refetch} = useMailsQuery();
  const totalEmails = data?.pages?.[0]?.total || 0;

  return (
    <>
      <Box
        sx={{
          p: 2,
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{alignItems: "center", gap: 1}}>
          <Typography level="title-lg" textColor="text.secondary">
                        My inbox
          </Typography>
          <Typography level="title-sm" textColor="text.tertiary">
            {totalEmails || "All"} emails for the last week
          </Typography>
        </Box>
        <Box>
          <Button
            size="sm"
            variant="soft"
            disabled={isRefetching}
            onClick={() => refetch()}
            sx={{ml: "auto", mr: 2}}
          >
            {isRefetching ? <CircularProgress size="sm"/> : <Refresh/>}
          </Button>
          <Button
            size="sm"
            startDecorator={<CreateRoundedIcon/>}
            onClick={() => mailManager?.composeMail({})}
            sx={{ml: "auto"}}
          >
                        Compose email
          </Button>
        </Box>
      </Box>
      <List
        sx={{
          height: "85.3vh",
          overflowY: "scroll",
          [`& .${listItemButtonClasses.root}.${listItemButtonClasses.selected}`]: {
            borderLeft: "2px solid",
            borderLeftColor: "var(--joy-palette-primary-outlinedBorder)",
          },
        }}
      >
        {isLoading && <LinearProgress/>}
        {data?.pages && data.pages.map((page) => (
          <React.Fragment key={page.page}>
            {page.data.map((mail) => (
              <React.Fragment key={mail.id}>
                <ListItem onClick={chooseEmail(mail)}>
                  <ListItemButton
                    {...(mail.id === selectedMail?.id && {
                      selected: true,
                      color: "neutral",
                    })}
                    sx={{p: 2}}
                  >
                    {/*<ListItemDecorator sx={{ alignSelf: 'flex-start' }}>*/}
                    {/*  <Avatar alt="" srcSet="https://i.pravatar.cc/80?img=3" src="https://i.pravatar.cc/40?img=3" />*/}
                    {/*</ListItemDecorator>*/}
                    <Box sx={{pl: 0, width: "100%"}}>
                      <Box
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                          mb: 0.5,
                        }}
                      >
                        <Box sx={{display: "flex", alignItems: "center", gap: 0.5}}>
                          <Typography level="body-xs">{mail.from}</Typography>
                          <Box
                            sx={{
                              width: "8px",
                              height: "8px",
                              borderRadius: "99px",
                              bgcolor: "primary.500",
                            }}
                          />
                        </Box>
                        <Typography level="body-xs" textColor="text.tertiary">
                          {new Date(mail.date).toDateString()}
                        </Typography>
                      </Box>
                      <div>
                        <Typography level="title-sm" sx={{mb: 0.5}}>
                          {mail.subject}
                        </Typography>
                        <Typography level="body-sm">{truncate(mail.body)}</Typography>
                      </div>
                    </Box>
                  </ListItemButton>
                </ListItem>
                <ListDivider sx={{m: 0}}/>
              </React.Fragment>
            ))}
          </React.Fragment>
        ))}

        <Button onClick={() => fetchNextPage()} disabled={!hasNextPage || isFetchingNextPage} sx={{m: 2}}>
          {isFetchingNextPage
            ? "Loading more..."
            : hasNextPage
              ? "Load More"
              : "Nothing more to load"}
        </Button>
      </List>
    </>
  );
}
