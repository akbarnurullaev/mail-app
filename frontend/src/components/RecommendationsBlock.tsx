import Layout from "./Layout.tsx";
import EmailContent from "./EmailContent.tsx";
import {Box, Chip} from "@mui/joy";
import {useState} from "react";
import {Mail} from "../api/hooks/useMails.ts";
import {Recommendations} from "./Recommendations.tsx";

export const RecommendationsBlock = () => {
  const [mail, setMail] = useState<Mail>();
  const [open, setOpen] = useState(false);

  const chooseEmail = (mail: Mail) => {
    return () => setMail(mail);
  };

  return (
    <>
      <Layout.SidePane>
        <Recommendations open={open} setOpen={setOpen} chooseEmail={chooseEmail}
          selectedMail={mail}/>
      </Layout.SidePane>
      <Layout.Main sx={{
        height: "93.5vh",
        overflowY: "scroll",
      }}>

        {mail ?
          <EmailContent mail={mail}/> :
          <Box sx={{
            display: "flex",
            height: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}>
            <Chip size="md" variant="soft" color="primary">
                            Select an email to see the content
            </Chip>
          </Box>}
      </Layout.Main>
    </>
  );
};