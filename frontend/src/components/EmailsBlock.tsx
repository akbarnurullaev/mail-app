import Layout from "./Layout.tsx";
import Mails from "./Mails.tsx";
import EmailContent from "./EmailContent.tsx";
import {Box, Chip} from "@mui/joy";
import {Mail} from "../api/hooks/useMails.ts";
import {useCompose} from "../providers/ComposeContext.tsx";

export const EmailsBlock = () => {
  const mailComposer = useCompose();
  const chooseEmail = (mail: Mail) => {
    return () => mailComposer?.setSelectedMail(mail);
  };

  return (
    <>
      <Layout.SidePane>
        <Mails chooseEmail={chooseEmail}
          selectedMail={mailComposer?.selectedMail}/>
      </Layout.SidePane>
      <Layout.Main sx={{
        height: "93.5vh",
        overflowY: "scroll",
      }}>

        {mailComposer?.selectedMail ?
          <EmailContent mail={mailComposer.selectedMail}/> :
          <Box sx={{
            display: "flex",
            height: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}>
            <Chip size="md" variant="soft" color="primary">
                            Select an email to see the content
            </Chip>
          </Box>}
      </Layout.Main>
    </>
  );
};