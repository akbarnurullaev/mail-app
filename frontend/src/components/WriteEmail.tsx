import * as React from "react";
import {useEffect} from "react";
import Box from "@mui/joy/Box";
import ModalClose from "@mui/joy/ModalClose";
import Button from "@mui/joy/Button";
import FormControl from "@mui/joy/FormControl";
import FormLabel from "@mui/joy/FormLabel";
import Textarea from "@mui/joy/Textarea";
import Sheet from "@mui/joy/Sheet";
import {Autocomplete, Chip, IconButton, Input, Stack, Typography} from "@mui/joy";

import FormatColorTextRoundedIcon from "@mui/icons-material/FormatColorTextRounded";
import AttachFileRoundedIcon from "@mui/icons-material/AttachFileRounded";
import InsertPhotoRoundedIcon from "@mui/icons-material/InsertPhotoRounded";
import FormatListBulletedRoundedIcon from "@mui/icons-material/FormatListBulletedRounded";
import {Mail} from "../api/hooks/useMails.ts";
import {useComposeMail} from "../api/hooks/useComposeMail.ts";
import {Close} from "@mui/icons-material";
import {useContactsQuery} from "../api/hooks/useContacts.ts";
import {useForm} from "react-hook-form";


export type MailInput = Partial<Pick<Mail, "subject" | "body" | "to" | "cc">> & { reply?: boolean }

interface WriteEmailProps {
    parentMail?: Mail;
    open?: boolean;
    mail?: MailInput;
    onClose?: () => void;
}

const WriteEmail = React.forwardRef<HTMLDivElement, WriteEmailProps>(
  function WriteEmail({open, onClose, mail, parentMail}, ref) {
    const contacts = useContactsQuery();
    const {register, handleSubmit, setValue, reset, watch, formState} = useForm<MailInput>();
    const to = watch("to") || [];
    const cc = watch("cc") || [];
    const mailComposition = useComposeMail(() => {
      onClose?.();
      reset();
    });
    const disabled = mailComposition.isLoading || !formState.isValid || !to?.length;

    const sendMail = handleSubmit((data) => mailComposition.mutate({...data, ...(mail?.reply ? {parentMail} : undefined)}));

    useEffect(() => {
      setValue("subject", mail?.subject);
      setValue("to", mail?.to);
      setValue("cc", mail?.cc);

      const body = mail?.body;

      let i = 0;

      function typeWriter() {
        if (body && i < body.length) {
          setValue("body", body.slice(0, i) + body.charAt(i));
          i++;
          setTimeout(typeWriter, 25);
        }
      }

      typeWriter();


      setValue("body", mail?.body);
    }, [mail]);


    return (
      <Sheet
        ref={ref}
        sx={{
          alignItems: "center",
          px: 1.5,
          py: 1.5,
          ml: "auto",
          width: {xs: "100dvw", md: 600},
          flexGrow: 1,
          border: "1px solid",
          borderRadius: "8px 8px 0 0",
          backgroundColor: "background.level1",
          borderColor: "neutral.outlinedBorder",
          boxShadow: "0px 8px 32px 8px rgba(220, 220, 220, 1)",
          zIndex: 1000,
          position: "fixed",
          bottom: 0,
          left: 24,
          transform: open ? "translateY(0)" : "translateY(100%)",
          transition: "transform 0.3s ease",
        }}
      >
        <Box sx={{mb: 2}}>
          <Typography level="title-sm">New message</Typography>
          <ModalClose id="close-icon" onClick={onClose}/>
        </Box>
        <Box
          sx={{display: "flex", flexDirection: "column", gap: 2, flexShrink: 0}}
        >
          <FormControl>
            <FormLabel>To</FormLabel>
            {/*<Input placeholder="email@email.com" aria-label="Message"/>*/}
            <Autocomplete
              multiple
              freeSolo
              onChange={(_, value,) => {
                setValue("to", value);
              }}
              value={to}
              placeholder="email@email.com"
              options={contacts.data || []}
              loading={contacts.isLoading}
              getOptionLabel={(option) => option}
              renderGroup={(tags, getTagProps) =>
                tags.map((item, index) => (
                  <Chip
                    key={item}
                    variant="solid"
                    color="primary"
                    endDecorator={<Close fontSize="small"/>}
                    {...getTagProps({index})}
                  >
                    {item}
                  </Chip>
                ))}
            />
          </FormControl>
          <FormControl>
            <FormLabel>Cc</FormLabel>
            <Autocomplete
              multiple
              freeSolo
              onChange={(_, value,) => {
                setValue("cc", value);
              }}
              value={cc}
              placeholder="email@email.com"
              options={contacts.data || []}
              loading={contacts.isLoading}
              getOptionLabel={(option) => option}
              renderGroup={(tags, getTagProps) =>
                tags.map((item, index) => (
                  <Chip
                    key={item}
                    variant="solid"
                    color="primary"
                    endDecorator={<Close fontSize="small"/>}
                    {...getTagProps({index})}
                  >
                    {item}
                  </Chip>
                ))}
            />
          </FormControl>
          <Input {...register("subject", {required: true})} defaultValue={mail?.subject} placeholder="Subject"
            aria-label="Message"/>
          <FormControl sx={{display: "flex", flexDirection: "column", gap: 2}}>
            <Textarea
              {...register("body", {required: true})}
              placeholder="Type your message here…"
              aria-label="Message"
              minRows={8}
              defaultValue={mail?.body}
              endDecorator={
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="center"
                  flexGrow={1}
                  sx={{
                    py: 1,
                    pr: 1,
                    borderTop: "1px solid",
                    borderColor: "divider",
                  }}
                >
                  <div>
                    <IconButton size="sm" variant="plain" color="neutral">
                      <FormatColorTextRoundedIcon/>
                    </IconButton>
                    <IconButton size="sm" variant="plain" color="neutral">
                      <AttachFileRoundedIcon/>
                    </IconButton>
                    <IconButton size="sm" variant="plain" color="neutral">
                      <InsertPhotoRoundedIcon/>
                    </IconButton>
                    <IconButton size="sm" variant="plain" color="neutral">
                      <FormatListBulletedRoundedIcon/>
                    </IconButton>
                  </div>
                  <Button
                    color="primary"
                    sx={{borderRadius: "sm"}}
                    disabled={disabled}
                    onClick={sendMail}
                  >
                                        Send
                  </Button>
                </Stack>
              }
              sx={{
                "& textarea:first-of-type": {
                  minHeight: 72,
                },
              }}
            />
          </FormControl>
        </Box>
      </Sheet>
    );
  },
);

export default WriteEmail;
