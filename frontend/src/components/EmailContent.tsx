import * as React from "react";
import Box from "@mui/joy/Box";
import Chip from "@mui/joy/Chip";
import Sheet from "@mui/joy/Sheet";
import Typography from "@mui/joy/Typography";
import Button from "@mui/joy/Button";
import Snackbar from "@mui/joy/Snackbar";
import Divider from "@mui/joy/Divider";
import Tooltip from "@mui/joy/Tooltip";

import DeleteRoundedIcon from "@mui/icons-material/DeleteRounded";
import ForwardToInboxRoundedIcon from "@mui/icons-material/ForwardToInboxRounded";
import ReplyRoundedIcon from "@mui/icons-material/ReplyRounded";
import CheckCircleRoundedIcon from "@mui/icons-material/CheckCircleRounded";
import {useCompose} from "../providers/ComposeContext.tsx";
import {ReplyAllRounded} from "@mui/icons-material";
import {Mail} from "../api/hooks/useMails.ts";
import {useMailHistoryGeneration} from "../api/hooks/useMailHistoryGeneration.ts";

export default function EmailContent({mail}: { mail: Mail }) {
  const mailManager = useCompose();
  const [open, setOpen] = React.useState([false, false, false]);
  const {mutateAsync} = useMailHistoryGeneration();

  const reply = async () => {
    // handleSnackbarOpen(0);
    let body = "";
    if (mailManager?.selectedMail?.body) {
      body = (await mutateAsync(mailManager?.selectedMail?.body)).data;
      console.log(body);
    }
    mailManager?.composeMail({
      subject: mail.subject, to: [mail?.from], body, reply: true
    });
  };

  const replyAll = async () => {
    // handleSnackbarOpen(0);
    let body = "";
    if (mailManager?.selectedMail?.body) {
      body = (await mutateAsync(mailManager?.selectedMail?.body)).data;
      console.log(body);
    }
    mailManager?.composeMail({
      subject: mail.subject, to: [mail?.from], body, cc: mail.cc, reply: true
    });
  };

  const handleSnackbarOpen = (index: number) => {
    const updatedOpen = [...open];
    updatedOpen[index] = true;
    setOpen(updatedOpen);
  };

  const handleSnackbarClose = (index: number) => {
    const updatedOpen = [...open];
    updatedOpen[index] = false;
    setOpen(updatedOpen);
  };

  return (
    <Sheet
      variant="outlined"
      sx={{
        borderRadius: "sm",
        p: 2,
        mb: 3,
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          flexWrap: "wrap",
          gap: 2,
        }}
      >
        <Box sx={{display: "flex", justifyContent: "space-between"}}>
          {/*<Avatar*/}
          {/*  src="https://i.pravatar.cc/40?img=3"*/}
          {/*  srcSet="https://i.pravatar.cc/80?img=3"*/}
          {/*/>*/}
          <Box>
            <Typography level="title-sm" textColor="text.primary" mb={0.5}>
              {mail.from}
            </Typography>
            <Typography level="body-xs" textColor="text.tertiary">
              {mail.date}
            </Typography>
          </Box>
        </Box>
        <Box
          sx={{display: "flex", height: "32px", flexDirection: "row", gap: 1.5}}
        >
          <Button
            size="sm"
            variant="plain"
            color="neutral"
            startDecorator={<ReplyRoundedIcon/>}
            onClick={reply}
          >
                        Reply
          </Button>
          <Snackbar
            color="success"
            open={open[0]}
            onClose={() => handleSnackbarClose(0)}
            anchorOrigin={{vertical: "bottom", horizontal: "right"}}
            startDecorator={<CheckCircleRoundedIcon/>}
            endDecorator={
              <Button
                onClick={() => handleSnackbarClose(0)}
                size="sm"
                variant="soft"
                color="neutral"
              >
                                Dismiss
              </Button>
            }
          >
                        Your message has been sent.
          </Snackbar>
          <Button
            size="sm"
            variant="plain"
            color="neutral"
            startDecorator={<ReplyAllRounded/>}
            onClick={replyAll}
          >
                        Reply All
          </Button>
          <Button
            size="sm"
            variant="plain"
            color="neutral"
            startDecorator={<ForwardToInboxRoundedIcon/>}
            onClick={() => handleSnackbarOpen(1)}
          >
                        Forward
          </Button>
          <Snackbar
            color="success"
            open={open[1]}
            onClose={() => handleSnackbarClose(1)}
            anchorOrigin={{vertical: "bottom", horizontal: "right"}}
            startDecorator={<CheckCircleRoundedIcon/>}
            endDecorator={
              <Button
                onClick={() => handleSnackbarClose(1)}
                size="sm"
                variant="soft"
                color="neutral"
              >
                                Dismiss
              </Button>
            }
          >
                        Your message has been forwarded.
          </Snackbar>
          <Button
            size="sm"
            variant="plain"
            color="danger"
            startDecorator={<DeleteRoundedIcon/>}
            onClick={() => handleSnackbarOpen(2)}
          >
                        Delete
          </Button>
          <Snackbar
            color="danger"
            open={open[2]}
            onClose={() => handleSnackbarClose(2)}
            anchorOrigin={{vertical: "bottom", horizontal: "right"}}
            startDecorator={<CheckCircleRoundedIcon/>}
            endDecorator={
              <Button
                onClick={() => handleSnackbarClose(2)}
                size="sm"
                variant="soft"
                color="neutral"
              >
                                Dismiss
              </Button>
            }
          >
                        Your message has been deleted.
          </Snackbar>
        </Box>
      </Box>

      <Box
        sx={{py: 2, display: "flex", flexDirection: "column", alignItems: "start"}}
      >
        <Typography
          level="title-lg"
          textColor="text.primary"
          endDecorator={
            <Chip component="span" size="sm" variant="outlined" color="warning">
                            Personal
            </Chip>
          }
        >
          {mail.subject}
        </Typography>
        <Box
          sx={{
            mt: 1,
            display: "flex",
            alignItems: "center",
            gap: 1,
            flexWrap: "wrap",
          }}
        >
          <div>
            <Typography
              component="span"
              level="body-sm"
              sx={{mr: 1, display: "inline-block"}}
            >
                            From
            </Typography>
            <Tooltip size="sm" title="Copy email" variant="outlined">
              <Chip size="sm" variant="soft" color="primary" onClick={() => {
              }}>
                {mail.from}
              </Chip>
            </Tooltip>
          </div>
          <div>
            <Typography
              component="span"
              level="body-sm"
              sx={{mr: 1, display: "inline-block"}}
            >
                            to
            </Typography>
            <Tooltip size="sm" title="Copy email" variant="outlined">
              <Chip size="sm" variant="soft" color="primary" onClick={() => {
              }}>
                {mail.to}
              </Chip>
            </Tooltip>
          </div>
        </Box>
      </Box>
      <Divider/>
      {/*<Typography level="body-sm" mt={2} mb={2}>*/}
      {/*{mail.body}*/}
      {/*<div dangerouslySetInnerHTML={{__html: mail.html}}></div>*/}
      <iframe srcDoc={mail.html}
        style={{border: 0, width: "100%", height: "70vh",}}></iframe>
      {/*</Typography>*/}

      {/*<Divider />*/}
      {/*<Typography level="title-sm" mt={2} mb={2}>*/}
      {/*  Attachments*/}
      {/*</Typography>*/}
      {/*<Box*/}
      {/*  sx={(theme) => ({*/}
      {/*    display: 'flex',*/}
      {/*    flexWrap: 'wrap',*/}
      {/*    gap: 2,*/}
      {/*    '& > div': {*/}
      {/*      boxShadow: 'none',*/}
      {/*      '--Card-padding': '0px',*/}
      {/*      '--Card-radius': theme.vars.radius.sm,*/}
      {/*    },*/}
      {/*  })}*/}
      {/*>*/}
      {/*  <Card variant="outlined">*/}
      {/*    <AspectRatio ratio="1" sx={{ minWidth: 80 }}>*/}
      {/*      <img*/}
      {/*        src="https://images.unsplash.com/photo-1527549993586-dff825b37782?auto=format&h=80"*/}
      {/*        srcSet="https://images.unsplash.com/photo-1527549993586-dff825b37782?auto=format&h=160 2x"*/}
      {/*        alt="Yosemite National Park"*/}
      {/*      />*/}
      {/*    </AspectRatio>*/}
      {/*  </Card>*/}
      {/*  <Card variant="outlined">*/}
      {/*    <AspectRatio ratio="1" sx={{ minWidth: 80 }}>*/}
      {/*      <img*/}
      {/*        src="https://images.unsplash.com/photo-1532614338840-ab30cf10ed36?auto=format&h=80"*/}
      {/*        srcSet="https://images.unsplash.com/photo-1532614338840-ab30cf10ed36?auto=format&h=160 2x"*/}
      {/*        alt="Yosemite National Park"*/}
      {/*      />*/}
      {/*    </AspectRatio>*/}
      {/*  </Card>*/}
      {/*  <Card variant="outlined" orientation="horizontal">*/}
      {/*    <CardOverflow>*/}
      {/*      <AspectRatio ratio="1" sx={{ minWidth: 80 }}>*/}
      {/*        <div>*/}
      {/*          <FolderIcon />*/}
      {/*        </div>*/}
      {/*      </AspectRatio>*/}
      {/*    </CardOverflow>*/}
      {/*    <Box sx={{ py: { xs: 1, sm: 2 }, pr: 2 }}>*/}
      {/*      <Typography level="title-sm" color="primary">*/}
      {/*        videos-hike.zip*/}
      {/*      </Typography>*/}
      {/*      <Typography level="body-xs">100 MB</Typography>*/}
      {/*    </Box>*/}
      {/*  </Card>*/}
      {/*</Box>*/}
    </Sheet>
  );
}
