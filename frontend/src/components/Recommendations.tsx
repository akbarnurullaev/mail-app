import {Mail} from "../api/hooks/useMails.ts";
import Box from "@mui/joy/Box";
import Typography from "@mui/joy/Typography";
import {Button, LinearProgress} from "@mui/joy";
import {Refresh} from "@mui/icons-material";
import CreateRoundedIcon from "@mui/icons-material/CreateRounded";
import List from "@mui/joy/List";
import ListItemButton, {listItemButtonClasses} from "@mui/joy/ListItemButton";
import * as React from "react";
import ListItem from "@mui/joy/ListItem";
import {truncate} from "../helpers/truncate.ts";
import ListDivider from "@mui/joy/ListDivider";
import {useRecommendationsQuery} from "../api/hooks/useRecommendations.ts";
import {useCompose} from "../providers/ComposeContext.tsx";

export function Recommendations({chooseEmail, selectedMail}: {
    chooseEmail: (mail: Mail) => () => void,
    selectedMail?: Mail,
}) {
  const mailManager = useCompose();
  const {data, isLoading, refetch} = useRecommendationsQuery();
  const totalEmails = data?.length;

  return (
    <>
      <Box
        sx={{
          p: 2,
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{alignItems: "center", gap: 1}}>
          <Typography level="title-lg" textColor="text.secondary">
                        My inbox
          </Typography>
          <Typography level="title-sm" textColor="text.tertiary">
            {totalEmails || "All"} emails for the last week
          </Typography>
        </Box>
        <Box>
          <Button
            size="sm"
            variant="soft"
            onClick={() => refetch()}
            sx={{ml: "auto", mr: 2}}
          >
            <Refresh/>
          </Button>
          <Button
            size="sm"
            startDecorator={<CreateRoundedIcon/>}
            onClick={() => mailManager?.open()}
            sx={{ml: "auto"}}
          >
                        Compose email
          </Button>
        </Box>
      </Box>
      <List
        sx={{
          height: "85.3vh",
          overflowY: "scroll",
          [`& .${listItemButtonClasses.root}.${listItemButtonClasses.selected}`]: {
            borderLeft: "2px solid",
            borderLeftColor: "var(--joy-palette-primary-outlinedBorder)",
          },
        }}
      >
        {isLoading && <LinearProgress/>}
        {data?.length && data.map((mail) => (
          <React.Fragment key={mail.id}>
            <ListItem onClick={chooseEmail(mail)}>
              <ListItemButton
                {...(mail.id === selectedMail?.id && {
                  selected: true,
                  color: "neutral",
                })}
                sx={{p: 2}}
              >
                {/*<ListItemDecorator sx={{ alignSelf: 'flex-start' }}>*/}
                {/*  <Avatar alt="" srcSet="https://i.pravatar.cc/80?img=3" src="https://i.pravatar.cc/40?img=3" />*/}
                {/*</ListItemDecorator>*/}
                <Box sx={{pl: 0, width: "100%"}}>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      mb: 0.5,
                    }}
                  >
                    <Box sx={{display: "flex", alignItems: "center", gap: 0.5}}>
                      <Typography level="body-xs">{mail.from}</Typography>
                      <Box
                        sx={{
                          width: "8px",
                          height: "8px",
                          borderRadius: "99px",
                          bgcolor: "primary.500",
                        }}
                      />
                    </Box>
                    <Typography level="body-xs" textColor="text.tertiary">
                      {mail.date}
                    </Typography>
                  </Box>
                  <div>
                    <Typography level="title-sm" sx={{mb: 0.5}}>
                      {mail.subject}
                    </Typography>
                    <Typography level="body-sm">{truncate(mail.body)}</Typography>
                  </div>
                </Box>
              </ListItemButton>
            </ListItem>
            <ListDivider sx={{m: 0}}/>
          </React.Fragment>
        ))}
      </List>
    </>
  );
}
