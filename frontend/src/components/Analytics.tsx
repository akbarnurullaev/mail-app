import {LineChart} from "@mui/x-charts";
import Typography from "@mui/joy/Typography";
import Box from "@mui/joy/Box";
import {List} from "@mui/joy";
import ListItem from "@mui/joy/ListItem";
import ListItemDecorator from "@mui/joy/ListItemDecorator";
import {Mail} from "@mui/icons-material";

const urgentMailsData = [1, 2, 4, 0, 3, 0, 3];
const importantMailsData = [0, 4, 5, 0, 0, 5, 8];
const regularMailsData = [3, 12, 14, 10, 10, 2, 8];
const xLabels = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

export const Analytics = () => {
  return (
    <Box sx={{m: 2}}>
      <Typography level="title-lg" textColor="text.secondary">Analytics of mail for the last week</Typography>
      <List
        orientation="horizontal"
        component="nav"
        sx={{
          display: "flex",
          width: "100%",
          justifyContent: "space-between"
        }}
      >
        <ListItem>
          <ListItemDecorator>
            <Mail style={{color: "#4e79a7"}}/>
          </ListItemDecorator>
                    All mails: {regularMailsData.reduce((a, b) => a + b, 0)}
        </ListItem>
        <ListItem>
          <ListItemDecorator>
            <Mail style={{color: "#f28e2c"}}/>
          </ListItemDecorator>
                    Important mails: {importantMailsData.reduce((a, b) => a + b, 0)}
        </ListItem>
        <ListItem>
          <ListItemDecorator>
            <Mail style={{color: "#e15759"}}/>
          </ListItemDecorator>
                    Urgent mails: {urgentMailsData.reduce((a, b) => a + b, 0)}
        </ListItem>
      </List>

      <LineChart
        width={700}
        height={400}
        series={[
          {data: regularMailsData, color: "#4e79a7"},
          {data: importantMailsData, color: "#f28e2c"},
          {data: urgentMailsData, color: "#e15759"},
        ]}
        xAxis={[{scaleType: "point", data: xLabels}]}
      />
    </Box>
  );
};