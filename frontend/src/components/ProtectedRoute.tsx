import {useAuth} from "../providers/AuthContext.tsx";
import {Children} from "../types.ts";
import {Navigate} from "react-router-dom";

export const ProtectedRoute = ({ children }: Children) => {
  const auth = useAuth();

  if (!auth?.token || !auth.user) {
    return <Navigate to="/login" replace />;
  }

  return children;
};
