import sqlite3
import datetime as dt
from datetime import timedelta

from imap_tools import MailBox, AND
from openai import OpenAI

MAIL = 'akbarnurullayev24@gmail.com'
PASSWORD = 'pzah biwj kecl ebqv'

connection = sqlite3.connect('example.db')
cursor = connection.cursor()
client = OpenAI(
    organization='org-hQPQymA8vV5kRTfVaHn3HefI',
    api_key="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"
)

today = dt.date.today()
month_ago = dt.date(today.year - 1, 12, today.day) if today.month == 1 else dt.date(today.year, today.month - 1, today.day)
yesterday = today - timedelta(days=1)

cursor.execute("DROP TABLE IF EXISTS Mail")
cursor.execute('''
    CREATE TABLE Mail (
        id varchar(255),
        subject varchar(255),
        text text,
        html text,
        date text,
        from_ text,
        to_ text
    );
''')

with MailBox('imap.gmail.com').login(MAIL, PASSWORD) as mailbox:
    for msg in mailbox.fetch(criteria=AND(date_gte=yesterday), mark_seen=False, bulk=True):

        cursor.execute(f'''
            INSERT INTO Mail (id, subject, text, html, date, from_, to_)
            VALUES (
                '{msg.uid}', 
                '{msg.subject}', 
                '{msg.text.replace("'", '"')}',
                '{msg.html.replace("'", '"')}',
                '{msg.date}',
                '{msg.from_.replace("'", '"')}',
                '{''.join(msg.to)}'
            );
        ''')

cursor.execute('SELECT * FROM Mail;')
rows = cursor.fetchall()

for row in rows:
    print(row)
# response = client.chat.completions.create(
#     model="gpt-4",
#     messages=[
#         {
#             "role": "system",
#             "content": f"""
#                 Here is the python list with tuples of recent emails. Your goal is to
#                 analyze all of them and return the top 1 important one and
#                 also information with action items and/or approximated answer if needed.
#
#                 {rows[-3:]}
#                 """,
#         },
#     ],
# ).choices[0].message.content

# for row in rows[-6:]:
#     response = client.chat.completions.create(
#         model="gpt-4",
#         messages=[
#             {
#                 "role": "system",
#                 "content": """
#                     limit prose!!!
#
#                     Here is a tuple containing information about the email. Your goal is to
#                     analyze it and give information about in a json format:
#
#                     {
#                         relevanceAndClarity: // Relevance and Clarity,
#                         priorityLevel: // Priority Level: IMPORTANT or NOT IMPORTANT or null,
#                         sender: // Sender's Role and Relationship,
#                         sentimentAnalysis: // Sentiment Analysis,
#                         topicClassification: // Topic Classification,
#                         actionableItems: // Actionable Items,
#                         questionsAndQueries: // Questions and Queries,
#                         remindersAndFollowups: // Reminders and Follow-ups,
#                         briefing: // Briefing
#                         language: // Language Detection
#                         answerProposals: // Different Automated Answers if applicable with array of strings. If there is no then null.
#
#                     {row}
#                     """,
#             },
#         ],
#     ).choices[0].message.content
#     pprint(response)
#
#     print('======================')
#     print('    ')
#     print('    ')
#     print('    ')
#     print('    ')
#     print('    ')
#     print('    ')

# print(rows)

cursor.close()