import base64
from email.header import make_header, decode_header

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from openai import OpenAI
from redbox import EmailBox

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://127.0.0.1:5173",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def paginate(items, per_page):
    pages = [items[i:i + per_page] for i in range(0, len(items), per_page)]
    return {
        'total': len(items),
        'pages_count': len(pages),
        'pages': pages
    }


class MailFetcher:
    box = EmailBox(
        host='imap.gmail.com',
        port='993',
        username='akbarnurullayev24@gmail.com',
        password='pzah biwj kecl ebqv'
    )
    gtp_client = OpenAI(
        organization='org-hQPQymA8vV5kRTfVaHn3HefI',
        api_key="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"
    )
    mails = []

    def __init__(self):
        self.mails = self.box.inbox.search(unseen=True)[::-1]

    def fetch_mails(self):
        pass

    def paginate(self, per_page):
        # print(str(self.mails[0].date.strftime("%m/%d/%Y, %H:%M:%S")))
        return paginate(self.mails, per_page)


mailbox = MailFetcher()


@app.get("/")
def mails(per_page: int, page: int):
    mails_in_json = []
    pagination = mailbox.paginate(per_page)

    for mail in pagination['pages'][page]:
        date = '2022-12-01'
        id = mail.uid
        to = mail.to
        subject = str(make_header(decode_header(mail.subject)))
        from_ = str(make_header(decode_header(mail.from_)))

        try:
            body = base64.b64decode(mail.text_body).decode('utf-8')
        except:
            try:
                body = mail.text_body
            except:
                body = ''

        try:
            html = str(mail.html_body)
        except:
            try:
                html = str(mail.html_body)
            except:
                html = ''

        mails_in_json.append({
            'id': id,
            'subject': subject,
            'body': body,
            'html': html,
            'date': date,
            'from': from_,
            'to': to,
        })

    return {
        'data': mails_in_json,
        'mails_count': pagination['total'],
        'pages_count': pagination['pages_count']
    }
    # return 'wef'
# class MailComposition(BaseModel):
#     subject: str
#     body: str
#     to: List[str]
#     cc: List[str] = None


# for mail in mails[-10:]:
#     subject = str(make_header(decode_header(mail.subject)))
#     body = ''
#
#     try:
#         print(mail.text_body)
#         body = base64.b64decode(mail.text_body).decode('utf-8')
#     except:
#         try:
#             body = mail.text_body
#         except:
#             body = ''
#
#     if body:
#         response = client.chat.completions.create(
#             model="gpt-3.5-turbo",
#             messages=[
#                 {
#                     "role": "system",
#                     "content": f'''
#                         Here is information of the mail with subject and a body. Your goal is to
#                         analyze it and return action items, a brief preview, priority labels based on these
#                         options (Important, Urgent, Not Important, Not Urgent), sentiment label based on these
#                         options (Positive, Neutral, Negative), and response if it is Important.
#
#                         Your response should be in json format:
#
#                         actionItems: string[]
#                         brief: string
#                         priority: string
#                         sentiment: string
#                         response: string
#
#                         Subject: {subject}
#                         Body: {body}
#                         ''',
#                 },
#             ],
#         ).choices[0].message.content


# @app.get("/contacts")
# async def contacts():
#     return [
#         "galina.blokhina@skillbox.ru",
#         "translogistic.china@ya.ru",
#         "supermariorun-support@nintendo.co.jp",
#         "vlad.cohen@whirrcrew.com",
#         "ales.polacek@whirrcrew.com",
#         "beatrice.poskuviene@sa.vu.lt",
#         "gp-info@supercell.com",
#         "aleksandra.sheider@hyperskill.org",
#         "vladislav.cohen@outlook.com",
#         "agneta.lisauskiene@cr.vu.lt",
#         "akbarnurullayev24@gmail.com",
#         "admissions@cr.vu.lt",
#         "sergej.fedorovic@creativedock.com",
#         "jakhonali8@gmail.com",
#         "it@studentdepot.pl",
#         "vojtech.hejda@creativedock.com",
#         "developer-accounts@twitter.com",
#         "olegjaehn@gmail.com",
#         "rekrutacja@pja.edu.pl",
#         "ksiegowosc@pja.edu.pl",
#         "unsubscribe_7ad4188464914b96c29bd0e5789d21a4@smtp-pulse.com",
#         "talent@humans.net",
#         "reports@stel.com",
#         "taszkent.rk.wiza@msz.gov.pl",
#         "kontrakt@pja.edu.pl",
#         "kate.galitovska@whirrcrew.com",
#         "support@paysera.com",
#         "xkalcho@gmail.com",
#         "petr.mach@creativedock.com",
#         "warszawa@studentdepot.pl",
#         "alexjaehn24@gmail.com",
#         "info@codinggoats.com",
#         "vlad010694@gmail.com",
#         "kontrakt@pjwstk.edu.pl",
#         "flex.uz@americancouncils.org",
#         "sms@telegram.org",
#         "onboarding@whirrcrew.com",
#         "ales.zarsky@creativedock.com",
#         "accounting@whirrcrew.com",
#         "kontakt@studentdepot.pl",
#         "adela.brichackova@creativedock.com",
#         "s.dimak@gmail.com",
#         "rekrutacja@pjwstk.edu.pl",
#         "wilanowska@studentdepot.pl",
#         "pjatk@pja.edu.pl",
#         "recruitment@pja.edu.pl",
#         "manager@polsza.info"
#     ]
# TODO: Optimize and use the actual code
# with MailBox('imap.gmail.com').login(MAIL, PASSWORD, INBOX['sent']) as mailbox:
#     unique_senders = set()
#     criteria = 'ALL'
#
#     for mail in mailbox.fetch(criteria, bulk=True, reverse=True):
#         to = mail.to
#         for recipient in to:
#             unique_senders.add(recipient)
#
#     return unique_senders


# @app.get("/mails")
# async def mails(page: int = 0, per_page: int = 10, inbox: str = INBOX['inbox']):
#     with MailBox('imap.gmail.com').login(MAIL, PASSWORD, inbox) as mailbox:
#         fetched_mails = []
#         criteria = 'ALL'
#         total_mails_count = len(mailbox.numbers(criteria))
#         pages = int(total_mails_count // per_page) + 1 if total_mails_count % per_page else int(
#             total_mails_count // per_page)
#         page_limit = slice(page * per_page, page * per_page + per_page)
#
#         for mail in mailbox.fetch(criteria, bulk=True, reverse=True, limit=page_limit):
#             uid = mail.uid
#             to = mail.to
#             cc = mail.cc
#             subject = mail.subject
#             date_ = mail.date
#             from_ = mail.from_
#             body = mail.text
#             html = mail.html
#
#             if subject or body or to or from_:
#                 fetched_mails.append({
#                     'id': uid,
#                     'subject': subject,
#                     'body': body,
#                     'html': html,
#                     'date': date_,
#                     'from': from_,
#                     'to': to,
#                     'cc': cc,
#                 })
#
#         return {
#             'total': total_mails_count,
#             'page': page,
#             'pages_count': pages,
#             'data': fetched_mails
#         }
#
#
# @app.post("/compose")
# def compose(mail: MailComposition):
#     gmail.send(subject=mail.subject, html=mail.body, receivers=mail.to, cc=mail.cc)
#
#
# @app.post("/reply")
# def reply(mail_id: str, mail: MailComposition):
#     gmail.send(subject=mail.subject, html=mail.body, receivers=mail.to, cc=mail.cc,
#                headers={'In-Reply-To': mail_id, 'References': mail_id})

# @app.get("/recommendations")
# # async def recommendations(date_gte: str, from_: str, to_: str, limit: int):
# def recommendations():
#     response_mails = []
#     formatted_mails = fetch_mails(AND(answered=False, date_gte=yesterday, seen=False))
#
#     for mail in formatted_mails:
#         try:
#             gpt = client.chat.completions.create(
#                 model="gpt-3.5-turbo",
#                 messages=[
#                     {
#                         "role": "system",
#                         "content": f'''
#                             Limit prose!!!
#
#                             You are tasked with analyzing an email to determine its relevance, importance.
#                             Consider both the subject line and the body of the email. Provide insights into
#                             the key topics, potential urgency, and any notable details that might help
#                             in classifying the email. Mail is relevant when it is related to projects, business and
#                             doesn't include any promotions, social activities, spam. Also generate potential response
#                             to this mail if it is urgent and important, don't if sender email contains no-reply.
#
#                             Your response should be in json format:
#
#                             actionItems: string[]
#                             overview: string
#                             urgent: boolean
#                             important: boolean
#                             response: string
#
#                             Subject: {mail['subject']}
#                             Body: {mail['body']}
#                         ''',
#                     },
#                 ],
#             ).choices[0].message.content
#             print(gpt)
#         except:
#             gpt = ''

#         try:
#             if gpt:
#                 gpt_response = json.loads(gpt)
#
#                 if gpt_response['important'] or gpt_response['urgent']:
#                     mail['gpt'] = gpt
#                     response_mails.append(mail)
#         except:
#             pass
#
#     return response_mails
