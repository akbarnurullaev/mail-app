from datetime import date, timedelta
from typing import List

from fastapi import FastAPI, HTTPException, Depends
from openai import OpenAI, OpenAIError
from pydantic import BaseModel
from redmail import gmail
from sqlalchemy.orm import Session
from starlette import status
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import HTMLResponse, StreamingResponse
from typing_extensions import Optional

import models
from database import get_db, engine, Base

MAIL = 'akbarnurullayev24@gmail.com'
PASSWORD = 'pzah biwj kecl ebqv'
INBOX = {
    'inbox': 'INBOX',
    'sent': '[Gmail]/Sent Mail'
}

client = OpenAI(
    organization='org-hQPQymA8vV5kRTfVaHn3HefI',
    api_key="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"
)
gmail.username = MAIL
gmail.password = PASSWORD
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

today = date.today()
yesterday = today - timedelta(days=1)
last_week = today - timedelta(days=7)


class MailHistoryAnalysis(BaseModel):
    history: str
    receivers: Optional[List[str]] = None
    me: Optional[str] = None
    subject: Optional[str] = None
    context: Optional[str] = None


class NoteBaseSchema(BaseModel):
    id: str | None = None
    text: str

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        arbitrary_types_allowed = True


@app.on_event("startup")
def startup():
    Base.metadata.create_all(bind=engine)


@app.post("/set_mail")
def set_mail(mail: MailHistoryAnalysis, db: Session = Depends(get_db)):
    receivers = "The receivers of the generated mail are " + ", ".join(mail.receivers) + "." if mail.receivers else ""
    author = "The account information of the generated mail is " + mail.me + "." if mail.me else ""
    subject = "Take into consideration the subject of the mail which is " + mail.subject if mail.subject else ""
    context = "Keep in mind the context information of the mail which is " + mail.context if mail.context else ""

    text = f'''
        Generate a brief well-formatted answer for the latest mail. However, keep in mind the whole 
        mail history for context. {receivers} {author} {subject}
        The body of the email should be divided into short and coherent paragraphs that convey the message 
        clearly and concisely. Each paragraph should focus on one main idea or point and support it with 
        relevant details, examples, or evidence. A concise paragraph helps the recipients follow the logic 
        and understand your intentions. Avoid long or complex sentences that might confuse or bore the recipients. 
        Instead, use simple and direct language that expresses your thoughts and feelings without ambiguity 
        or redundancy. Avoid using too much or too little white space that might make the email look too sparse or 
        too crowded. Return only the answer, not subject or anything. Don't include spacing before the email. 
        Get the tone, sentiment and the language of the chat based on the history and respond accordingly.
        
        {context}

        The mail history is this:
        ---
        {mail.history.replace("'", '"')}
        ---
        '''
    prompt = models.Prompt(text=text)

    db.add(prompt)
    db.commit()
    db.refresh(prompt)

    return prompt.id


@app.get("/streaming_gpt")
async def openai_streaming(id_: str, db: Session = Depends(get_db)) -> StreamingResponse:
    prompt = db.query(models.Prompt).filter(models.Prompt.id == id_).first()

    if not prompt:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No prompt with this id: {id} found")

    try:
        def event_stream():
            stream = client.chat.completions.create(
                model="gpt-4-0125-preview",
                messages=[
                    {
                        "role": "system",
                        "content": prompt.text,
                    },
                ],
                stream=True,
            )
            for chunk in stream:
                data = chunk.choices[0].delta.content
                data = data.replace("\n", "^") if data else data
                print(chunk.choices[0].delta, f"'{data}'", '\n')
                if data is not None:
                    yield f"data: {data} \n\n"
                else:
                    yield f"data:$ended$\n\n"

        return StreamingResponse(event_stream(), media_type="text/event-stream")

    except OpenAIError:
        raise HTTPException(status_code=500, detail='OpenAI call failed')


@app.post("/reply_gpt")
def reply_gpt(mail: MailHistoryAnalysis):
    try:
        response = client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[
                {
                    "role": "system",
                    "content": f'''
                        You are tasked with analysis of the mail history and generation of a relevant answer 
                        to the latest mail. Return only response text!!!.
                        {"The receivers of the generated mail are " + ", ".join(mail.receivers) if mail.receivers else ""}

                        {mail.history}
                    ''',
                },
            ],
        ).choices[0].message.content
        print(response)
    except:
        response = ''

    return response


@app.get("/")
async def read_items():
    html_content = """
    <html>
        <head>
            <title>AIReply - Future of AI within your mail inbox</title>
        </head>
        <body>
            <h1>Future of AI within your mail inbox</h1>
        </body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)


@app.get("/privacy-policy")
async def read_items():
    html_content = """
    <html>
        <head>
            <title>AIReply - Privacy Policy</title>
        </head>
        <body>
            <p>
                Privacy Policy for Gmail Reply Extension:

                Thank you for using our Gmail Reply Extension ("AIReply"). This Privacy Policy outlines how we collect, use, and safeguard your information when you use the Extension.

                Information We Collect:
                ActiveTab Information: The Extension requires access to the active Gmail tab to enhance the email reply experience. This includes basic information about the email being replied to, such as sender, recipient, and email content.

                Gmail Domain Information: To operate exclusively within Gmail, the Extension accesses the gmail.com domain. This access is limited to reading and modifying email content for the sole purpose of composing and sending replies.

                How We Use Your Information:
                Enhancing Email Replies: The information collected is used to improve the functionality of the Extension, ensuring a seamless and efficient email reply experience within the Gmail environment.

                Information Sharing:
                Third-Party Services: We do not share your information with third-party services. The Extension operates exclusively within the Gmail environment and does not transfer data to external entities, but for our serce to analyze the content. No data is saved!
                Data Security:
                Secure Access: The Extension is designed to access and interact with Gmail securely. We employ industry-standard security measures to protect your data during its use within the Extension.
                Your Choices:
                Permissions Control: You can manage permissions for the Extension in your browser settings. The Extension only requests the necessary permissions to fulfill its core functionality of enhancing Gmail replies.
                Updates to the Privacy Policy:
                Policy Changes: We may update this Privacy Policy to reflect changes in the Extension or legal requirements. Updates will be effective upon posting the revised Privacy Policy.
                Contact Us:
                If you have any questions or concerns about this Privacy Policy or the Extension, please contact us at akbarnurullayev24@gmail.com.

                By using the Extension, you agree to the terms outlined in this Privacy Policy. It is important to review this Privacy Policy periodically for any updates.
            </p>   
        </body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)
